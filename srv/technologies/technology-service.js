module.exports = (srv) => {

    const { Sizes } = cds.entities;

    srv.on('READ', 'Sizes', (req) => {
        const keys = Object.keys(Sizes.elements.size.enum);
        let ret = [];
        keys.forEach( (key, index) => {
            ret.push({size: key})
        });
        return ret;
    });
}