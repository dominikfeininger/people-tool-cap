using {js.peopletool as my} from '../../db/schema';

service PeopleService /* @(requires:'superuser') */{
    @Capabilities : {
        Insertable : false,
        Updatable  : false,
        Deletable  : false
    }
    entity People as projection on my.People
}
