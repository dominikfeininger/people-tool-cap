using {js.peopletool as my} from '../../db/schema';

service PeopleSkillService {
    // import of other models
    entity People       as projection on my.People
    entity Skills       as projection on my.Skills
    // service - entity with one to one mapping
    entity PeopleSkills as projection on my.PeopleSkills
}
