

const app = require('../../app/app');

module.exports = (srv) => {

    srv.on('check', req => {
        
        const appInst = new app();
        return ` Test auth ${req.data.to}???; with: ` + appInst.auth();
    });
}