
service LoginService {
    function login(to : String) returns String;
    function auth(to : String) returns String;
}
