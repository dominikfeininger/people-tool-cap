// namespace com.js.peopletool;
namespace js.peopletool;

using {
    managed,
    cuid,
    Country,
    User
} from '@sap/cds/common';

entity People : cuid, managed {
    key ID            : Integer;
        // key ID            : UUID @odata.Type : 'Edm.String';
        name          : String;
        title         : String;
        hobby         : String;
        placeOfLiving : String;
        skills        : Composition of many PeopleSkills
                            on skills.people = $self;
// v2
// project: Association to many Projects on skill.ID = $self;
}

// many to many relation
entity PeopleSkills : cuid, managed {
    key people : Association to People;
    key skill  : Association to Skills;
// peopleComposition : String;
}

entity Skills : cuid, managed {
    key ID         : Integer;
        // people     : Association to People not null;
        name       : String;
        technology : String;
        descr      : String;
        owner      : String;
        experience : DateTime;
        // ownerPeople: Composition of many People on people.skillRef = $self;
        people     : Composition of many PeopleSkills
                         on people.skill = $self;
}

// v2
entity Projects : managed {
    key ID           : Integer;
        name         : String;
        effort       : Integer;
        businessArea : String;
        size         : Size;
}

// v3
entity Technologies : managed {
    key ID    : Integer;
        name  : String;
        level : Integer
}

// example Implentation
type Size : String(4) enum {
    XXS;
    XS;
    S;
    M;
    L;
    XL;
    XXL;
}

entity Tag {
    TagID : Integer;
    Title : Integer;
}

entity ItemTag {
    PeopleID : Integer;
    TagID    : Integer;
}

entity Sizes {
    key size : Size;
}
