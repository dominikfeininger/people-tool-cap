using PeopleSkillService from '../../srv/peopleskill/peopleskill-service';

annotate PeopleSkillService.Skills with @(UI : {
    // skills overview list
    SelectionFields : [name],
    LineItem        : [
    {
        Value : ID,
        Label : 'ID'
    },
    {
        Value : name,
        Label : 'Name'
    },
    {
        Value : technology,
        Label : 'Technology'
    } /* ,
     {
         Value : owner,
         Label : 'Owner'
     } */
    ],
    // skills details
    HeaderInfo      : {
        TypeName       : 'Skill',
        TypeNamePlural : 'Skills',
        Title          : {Value : name},
        Description    : {Value : name}
    },
    Identification  : [ //Is the main field group
    {
        Value : createdBy,
        Label : 'createdBy'
    },
    {
        Value : createdAt,
        Label : 'createdAt'
    },
    ],
    Facets          : [
    {
        $Type  : 'UI.ReferenceFacet',
        Label  : 'People',
        Target : 'people/@UI.LineItem'
    },
    {
        $Type  : 'UI.ReferenceFacet',
        Label  : 'Skills ',
        Target : '@UI.Identification'
    },
    ]
});

annotate PeopleSkillService.PeopleSkills with @(UI : {
    LineItem : [
    {
        Value : people_ID,
        Label : 'people_ID'
    },
    {
        Value : skill_ID,
        Label : 'skill_ID'
    },
    {
        Value : createdAt,
        Label : 'createdAt'
    }
    /*,
    {
        Value : peopleComposition,
        Label : 'peopleComposition'
    } */
    ],
    Facets   : [{
        $Type  : 'UI.ReferenceFacet',
        Label  : 'People',
        // name of replation on schema.cda / ui type
        Target : 'people/@UI.LineItem'
    }]
});

annotate PeopleSkillService.People with @(UI : {
    SelectionFields : [name],
    LineItem        : [
    {
        Value : ID,
        Label : 'ID'
    },
    {
        Value : name,
        Label : 'name'
    },
    {
        Value : title,
        Label : 'Title'
    },
    {
        Value : hobby,
        Label : 'Hobby'
    },
    ],
    HeaderInfo      : {
        TypeName       : 'People',
        TypeNamePlural : 'People',
        Title          : {Value : name},
        Description    : {Value : title}
    },
});
