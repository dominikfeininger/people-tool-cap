/*
  This model controls what gets served to Fiori frontends...
  import all ui annotations
*/

using from './skills/fiori-service';
using from './peopleskill/fiori-service';
using from './people/fiori-service';
