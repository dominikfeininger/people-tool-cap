using PeopleService from '../../srv/people/people-service';

annotate PeopleService.People with {
    ID @title : '{i18n>ID}'  @UI.HiddenFilter;
};

annotate People with @odata.draft.enabled;

annotate PeopleService.People with @(UI : {
    // people overview list
    // SelectionFields   : [name],
    SelectionFields   : [
    createdAt,
    createdBy
    ],
    LineItem          : [
    {
        Value : ID,
        Label : 'ID'
    },
    {
        Value : name,
        Label : 'Name'
    }
    ],
    HeaderFacets      : [{
        $Type  : 'UI.ReferenceFacet',
        Label  : 'Description',
        Target : '@UI.FieldGroup#Descr'
    }, ],
    FieldGroup #Descr : {Data : [
    {
        Value : placeOfLiving,
        Label : 'Place of Living'
    },
    {
        Value : name,
        Label : 'Name'
    }
    ]},
    Identification    : [ //Is the main field group
    {
        Value : createdBy,
        Label : 'createdBy'
    },
    {
        Value : createdAt,
        Label : 'createdAt'
    }
    ],
    Facets            : [
    {
        $Type  : 'UI.ReferenceFacet',
        Label  : 'Skills',
        // Target : '@UI.FieldGroup#Skills'
        Target : 'skills/@UI.LineItem'
    },
    {
        $Type  : 'UI.ReferenceFacet',
        Label  : 'Details',
        Target : '@UI.Identification'
    },
    ],
    // people details
    HeaderInfo        : {
        TypeName       : 'People',
        TypeNamePlural : 'People',
        Title          : {
            Value : ID,
            Label : 'ID'
        },
        // Description    : {Value : title, Label: 'Title'},
        Description    : {Value : createdBy}
    // 'name is: ' + name + 'and skills are: ' + skills.name
    // `name is: ${name} and skills are: ${skills.name}}`
    },

/*     FieldGroup #Skills : {Data : {LineItem : [{
        Value : skills.name,
        Label : 'Skill Name'
    }, ]}}, */
});

annotate PeopleService.Skills with @(UI : {LineItem : [
{
    Value : ID,
    Label : 'ID'
},
{
    Value : name,
    Label : 'Name'
},
{
    Value : technology,
    Label : 'Technology'
}
]});
