const cds = require("@sap/cds");
// const skill = require("../skills/skill");
const { Skills } = cds.entities("js.peopletool");

module.exports = class skills {
    get(i) {
      return i;
    };
    auth() {
      return "auth in app";
    };
    async doStuff(data){
      if (data.ID === 1111) {
        return req.error(400, "Test successfull");
      }
      const tx = cds.transaction(req);
      const affectedRows = await tx.run(
        UPDATE(Skills)
          .set({ technology: "override, override, override" })
          .where({ ID: data.ID })
      );
      if (affectedRows === 1) {
        return req.error(409, "Invalid ID, sorry");
      }
    }
  };
  
  