using SkillService from '../../srv/skill/skill-service';


annotate SkillService.Skills with @(UI : {
    // skills overview list
    SelectionFields : [
    name,
    createdAt,
    createdBy
    ],
    LineItem        : [
    {
        Value : ID,
        Label : 'ID'
    },
    {
        Value : name,
        Label : 'Name'
    },
    {
        Value : technology,
        Label : 'Technology'
    },
    /* {
        Value : owner,
        Label : 'Owner'
    },
    {
        Value : people,
        Label : 'People'
    } */
    ],
    // skills details
    HeaderInfo      : {
        TypeName       : 'Skill',
        TypeNamePlural : 'Skills',
        Title          : {Value : name},
        Description    : {Value : name}
    },
    Identification  : [ //Is the main field group
    {
        Value : createdBy,
        Label : 'createdBy'
    },
    {
        Value : createdAt,
        Label : 'createdAt'
    },
    ],
    Facets          : [
    {
        $Type  : 'UI.ReferenceFacet',
        Label  : 'People',
        Target : 'people/@UI.LineItem'
    },
    {
        $Type  : 'UI.ReferenceFacet',
        Label  : 'Skills ',
        Target : '@UI.Identification'
    },
    ]
}) {
    createdAt @UI.HiddenFilter : false;
    createdBy @UI.HiddenFilter : false;
};

annotate SkillService.People with @(UI : {LineItem : [
{
    Value : ID,
    Label : 'ID'
},
{
    Value : name,
    Label : 'name'
}
]});

annotate SkillService.PeopleSkills with @(UI : {LineItem : [
{
    Value : people_ID,
    Label : 'people_ID'
},
{
    Value : skill_ID,
    Label : 'skill_ID'
} /* ,
 {
     Value : peopleComposition,
     Label : 'peopleComposition'
 } */
],

});
